require('dotenv').config()
const express = require('express')
// const {User} = require('./schema')
import User from './schema'
const app = express()
const port = 3000

console.log(`app storage set to ${process.env.APP_STORAGE}`)
if (process.env.APP_STORAGE == "memory") {
  let users = []
  const newUser = new User(1,"fia");
  users.push(newUser)
}

// load middlewares
app.use(express.json())

// routes definitions
app.get('/users', (req, res) => {
  res.send(users)
})

// TODO: add parameter validation later
app.post('/user', (req, res, next) => {
  const myuser = new User(req.body.id, req.body.name)
  users.push(myuser)
  res.send()
});

app.get('/user/:id', function (req, res) {
  const hit = users.find(element => element.id == req.params.id)
  res.send(hit)
})

// deployment
app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})
